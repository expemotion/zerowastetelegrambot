package projectweek9;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

public class Answers {
    private Document document;
    private String url;

    public Answers(String url) {
        this.url = url;
        connect();
    }

    private void connect(){
        try{
            document = Jsoup.connect("https://zws.moscow/blogs/blog/zero-waste-sovety-1").get();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public String getDescription(){
        Elements element = document.getElementsByClass("js-mediator-article");
        return element.text();
    }



}