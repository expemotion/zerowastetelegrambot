package projectweek9;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;

public class Bot extends TelegramLongPollingBot {
    ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
    long chat_id;
    String lastMessage = "";

    public void onUpdateReceived(Update update) {
        chat_id = update.getMessage().getChatId();

        SendMessage sendMessage = new SendMessage()
                .setChatId(chat_id)
                .setText(getMessage(update.getMessage().getText()));
        sendMessage.setReplyMarkup(replyKeyboardMarkup);

        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public String getMessage(String msg) {
        ArrayList<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        KeyboardRow keyboardSecondRow = new KeyboardRow();
        KeyboardRow keyboardThirdRow = new KeyboardRow();
        KeyboardRow keyboardFourthRow = new KeyboardRow();


        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        if (msg.equals("Привет") || msg.equals("Меню") || msg.equals("/start")) {
            keyboard.clear();
            keyboardFirstRow.add("Да");
            keyboardFirstRow.add("Нет");
            keyboard.add(keyboardFirstRow);
            replyKeyboardMarkup.setKeyboard(keyboard);
            return "Вы знакомы с таким образом жизни как Zero waste?";
        }

        if (msg.equals("Нет")) {
            keyboard.clear();
            keyboardFirstRow.add("Что это такое?");
            keyboardSecondRow.add("Важность");
            keyboardThirdRow.add("Принципы");
            keyboardFourthRow.add("Меню");
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);
            keyboard.add(keyboardThirdRow);
            keyboard.add(keyboardFourthRow);
            replyKeyboardMarkup.setKeyboard(keyboard);
            return "Выберите пункт меню";
        }

        if (msg.equals("Да")) {
            keyboard.clear();
            keyboardFirstRow.add("Эко пункты");
            keyboardSecondRow.add("Сэконд хэнды");
            keyboardThirdRow.add("Магазины");
            keyboardFourthRow.add("Меню");
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);
            keyboard.add(keyboardThirdRow);
            keyboard.add(keyboardFourthRow);
            replyKeyboardMarkup.setKeyboard(keyboard);
            return "Выберите пункт меню";
        }

        if (msg.equals("Принципы")) {
            keyboard.clear();
            keyboardFirstRow.add("refuse");
            keyboardFirstRow.add("reduce");
            keyboardSecondRow.add("reuse");
            keyboardSecondRow.add("recycle ");
            keyboardThirdRow.add("rot");
            keyboardThirdRow.add("Меню");
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);
            keyboard.add(keyboardThirdRow);
            replyKeyboardMarkup.setKeyboard(keyboard);
            return "Выберите пункт меню";
        }

        if (msg.equals("Эко пункты") || msg.equals("Сэконд хэнды") || msg.equals("Магазины")) {
            lastMessage = msg;
            keyboard.clear();
            keyboardFirstRow.add("Астана");
            keyboardFirstRow.add("Алматы");
            keyboardFirstRow.add("Меню");
            keyboard.add(keyboardFirstRow);
            replyKeyboardMarkup.setKeyboard(keyboard);
            return "С какого вы города?";
        }
        if (msg.equals("Астана")) {
            return "По этому пункту пока недостаточно информации";
        }
        if (msg.equals("Алматы")) {
            return "По этому пункту пока недостаточно информации";
        }

        return "Если возникли проблемы, воспользуйтесь /start";
    }


    public String getBotUsername() {
        return "zerowastels_bot";
    }

    public String getBotToken() {
        return "1090196075:AAGp0IgK0VUAXm-VKY9g5OtZ0bA2FzOtuek";
    }

}